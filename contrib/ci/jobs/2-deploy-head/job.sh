#!/bin/bash
set -exuo pipefail

ARTIFACT_PATH="/artifacts/sandcastle-ng/${CI_COMMIT_REF}/*.tar"

RSYNC_HOST="firefly.gnunet.org"
RSYNC_PORT=22
RSYNC_PATH="incoming"
RSYNC_USER="taler-head"
RSYNC_DEST="${RSYNC_USER}@${RSYNC_HOST}:${RSYNC_PATH}"

# Useful to print for debug
ls -alh $(dirname ${ARTIFACT_PATH})

## We need ssh and rsync
apt-get update -yqq
apt-get install -yqq --no-install-recommends \
	ssh-client rsync

# Send our container tarball to the host
rsync -vP \
      --port ${RSYNC_PORT} \
      ${ARTIFACT_PATH} ${RSYNC_DEST} || (rm -f ${ARTIFACT_PATH} && exit 1)

# Make sure we cleanup
rm -f ${ARTIFACT_PATH}
