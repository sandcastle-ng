#!/usr/bin/env bash

# This scripts provisions all configuration and
# services for the Taler sandcastle container.
#
# Important: This script needs to be completely
# idempotent, nothing must break if it is executed
# multiple times.

set -eu
set -x

if [[ -n ${SANDCASTLE_SKIP_SETUP:-} ]]; then
  echo "skipping sandcastle setup, requested by environment var SANDCASTLE_SKIP_SETUP"
  exit 1
fi

echo "Provisioning sandcastle"

# General configuration.
# Might eventually be moved to an external file.

# Source any overrides from external file
if [[ -e /overrides ]]; then
  source /overrides
fi

# When serving on an external port (for localhost deployments),
# we use http.
if [[ ${EXTERNAL_PORT:-} =~ ^[0-9]+$ ]]; then
  PROTO=http
  PORT_SUFFIX=:$EXTERNAL_PORT
else
  PROTO=https
  PORT_SUFFIX=
fi

CURRENCY=${CURRENCY:="KUDOS"}
EXCHANGE_IBAN=DE159593
EXCHANGE_PLAIN_PAYTO=payto://iban/$EXCHANGE_IBAN
EXCHANGE_FULL_PAYTO="payto://iban/$EXCHANGE_IBAN?receiver-name=Sandcastle+Echange+Inc"

# Randomly generated IBANs for the merchants
MERCHANT_IBAN_DEFAULT=DE5135717
MERCHANT_IBAN_POS=DE4218710
MERCHANT_IBAN_BLOG=DE8292195
MERCHANT_IBAN_GNUNET=DE9709960
MERCHANT_IBAN_TALER=DE1740597
MERCHANT_IBAN_TOR=DE2648777
MERCHANT_IBAN_SANDBOX=DE949115029592

MYDOMAIN=${MYDOMAIN:="demo.taler.net"}
LANDING_DOMAIN=$MYDOMAIN
BANK_DOMAIN=bank.$MYDOMAIN
EXCHANGE_DOMAIN=exchange.$MYDOMAIN
MERCHANT_DOMAIN=backend.$MYDOMAIN
BLOG_DOMAIN=shop.$MYDOMAIN
DONATIONS_DOMAIN=donations.$MYDOMAIN
CHALLENGER_DOMAIN=challenger.$MYDOMAIN
AUDITOR_DOMAIN=auditor.$MYDOMAIN

# Ports of the services running inside the container.
# Should be synchronized with the sandcastle-run script.
PORT_INTERNAL_EXCHANGE=8201
PORT_INTERNAL_MERCHANT=8301
PORT_INTERNAL_LIBEUFIN_BANK=8080
PORT_INTERNAL_LANDING=8501
PORT_INTERNAL_BLOG=8502
PORT_INTERNAL_DONATIONS=8503
PORT_INTERNAL_BANK_SPA=8505
PORT_INTERNAL_CHALLENGER=8506
PORT_INTERNAL_AUDITOR=8507

ENABLE_AUDITOR=0

# Just make sure the services are stopped
systemctl stop postgresql.service
systemctl stop taler-auditor.target
systemctl stop taler-exchange.target
systemctl stop taler-exchange-offline.timer
systemctl stop taler-merchant-httpd.service
systemctl stop taler-merchant.target
systemctl stop taler-demo-landing.service
systemctl stop taler-demo-blog.service
systemctl stop taler-demo-donations.service
systemctl stop libeufin-bank.service

# libeufin-nexus is not used
systemctl stop libeufin-nexus-ebics-fetch.service
systemctl disable libeufin-nexus-ebics-fetch.service
systemctl stop libeufin-nexus-ebics-submit.service
systemctl disable libeufin-nexus-ebics-submit.service

systemctl reset-failed

# We now make sure that some important locations are symlinked to
# the persistent storage volume.
# Files that already exist in this location are moved to the storage volume
# and then symlinked.
# These locations are:
# /etc/taler
# /etc/libeufin
# /var/lib/taler
# postgres DB directory

function lift_dir() {
  where=$1
  src=$2
  target=$3
  if [[ -L $src ]]; then
    # be idempotent
    echo "$src is already a symlink"
  elif [[ -d /$where/$target ]]; then
    echo "symlinking existing /$where/$target"
    rm -rf "$src"
    ln -s "/$where/$target" "$src"
  else
    echo "symlinking new /$where/$target"
    mv "$src" "/$where/$target"
    ln -s "/$where/$target" "$src"
  fi
}

lift_dir talerdata /var/lib/taler-exchange var-lib-taler-exchange
lift_dir talerdata /etc/taler-merchant etc-taler-merchant
lift_dir talerdata /etc/taler-exchange etc-taler-exchange
lift_dir talerdata /etc/taler-exchange etc-taler-auditor
lift_dir talerdata /etc/libeufin etc-libeufin
lift_dir talerdata /var/lib/postgresql var-lib-postgresql
lift_dir talerdata_persistent /var/lib/taler-exchange/offline exchange-offline


# Usage: get_credential_pw COMPONENT/ACCOUNT
function get_credential_pw() {
  if [[ ${USE_INSECURE_SANDBOX_PASSWORDS:-0} = 1 ]]; then
    echo "sandbox"
    return
  fi
  p=/credentials/$1
  if [[ ! -f $p ]]; then
    mkdir -p $(dirname "$p")
    uuidgen -r >$p
  fi
  cat "$p"
}

# If necessary, import the offline key.
# Done before everything else, as we need the key
# to generate the config.

if [[ -d /exported && -e /exported/import-request ]]; then
  echo "Importing exchange offline key"
  rm -rf /var/lib/taler-exchange/offline/*
  cp -r /exported/taler-exchange/offline/* /var/lib/taler-exchange/offline/
fi

# Adjust permissions
chown --recursive taler-exchange-offline:taler-exchange-offline /var/lib/taler-exchange/offline/* || true


MASTER_PUBLIC_KEY=$(sudo -i -u taler-exchange-offline taler-exchange-offline -LDEBUG setup)


#
# Create the basic configuration files
#

mkdir -p /etc/challenger/conf.d
cat <<EOF >/etc/challenger/conf.d/setup-sandcastle.conf
[challenger]
ADDRESS_TYPE = email
AUTH_COMMAND = /data/sandcastle-challenger-auth
ADDRESS_RESTRICTIONS = {"email":{"hint":"not an e-mail address","regex":"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"}}
EOF

cat <<EOF >/etc/libeufin/libeufin-bank.conf
[libeufin-bank]
CURRENCY = $CURRENCY
DEFAULT_DEBT_LIMIT = $CURRENCY:500
REGISTRATION_BONUS = $CURRENCY:100
SPA_CAPTCHA_URL = $PROTO://$BANK_DOMAIN$PORT_SUFFIX/webui/#/operation/{woid}
SUGGESTED_WITHDRAWAL_EXCHANGE = $PROTO://$EXCHANGE_DOMAIN$PORT_SUFFIX/
ALLOW_REGISTRATION = yes
SERVE = tcp
PORT = 8080
# Bind address.
# Option soon to be deprecated!
ADDRESS = 0.0.0.0

# Compat mode for now
PWD_CHECK = no
PWD_AUTH_COMPAT = yes

[currency-$CURRENCY]
ENABLED = YES
name = "${NAME:=Kudos}"
code = "$CURRENCY"
decimal_separator = "."
fractional_input_digits = ${FRACTIONALS:=2}
fractional_normal_digits = ${FRACTIONALS:=2}
fractional_trailing_zero_digits = ${FRACTIONALS:=2}
is_currency_name_leading = NO
alt_unit_names = {"0":"${ALT_UNIT_NAME:=ク}"}
EOF

cat <<EOF >/etc/libeufin/settings.json
{
  "topNavSites": {
    "Landing": "$PROTO://$LANDING_DOMAIN$PORT_SUFFIX/",
    "Bank": "$PROTO://$BANK_DOMAIN$PORT_SUFFIX",
    "Essay Shop": "$PROTO://$BLOG_DOMAIN$PORT_SUFFIX",
    "Donations": "$PROTO://$DONATIONS_DOMAIN$PORT_SUFFIX"
  }
}
EOF

# Generate /tmp/sandcastle-setup.conf
cat <<EOF >/tmp/sandcastle-setup.conf
[currency-$CURRENCY]
ENABLED = YES
name = "${NAME:=Kudos}"
code = "$CURRENCY"
decimal_separator = "."
fractional_input_digits = ${FRACTIONALS:=2}
fractional_normal_digits = ${FRACTIONALS:=2}
fractional_trailing_zero_digits = ${FRACTIONALS:=2}
is_currency_name_leading = NO
alt_unit_names = {"0":"${ALT_UNIT_NAME:=ク}"}
EOF

cp /tmp/sandcastle-setup.conf /etc/taler-exchange/conf.d/sandcastle-setup.conf
cp /tmp/sandcastle-setup.conf /etc/taler-merchant/conf.d/sandcastle-setup.conf


cat <<EOF >/etc/taler-exchange/conf.d/sandcastle-exchange.conf
[exchange]
CURRENCY = $CURRENCY
CURRENCY_ROUND_UNIT = $CURRENCY:0.01
AML_THRESHOLD = $CURRENCY:1000000
MASTER_PUBLIC_KEY = $MASTER_PUBLIC_KEY
BASE_URL = $PROTO://$EXCHANGE_DOMAIN$PORT_SUFFIX/

[taler-exchange-secmod-rsa]
LOOKAHEAD_SIGN = 4 weeks

[taler-exchange-secmod-eddsa]
LOOKAHEAD_SIGN = 4 weeks

[taler-exchange-secmod-cs]
LOOKAHEAD_SIGN = 4 weeks

[exchange-account-default]
PAYTO_URI = $EXCHANGE_FULL_PAYTO
ENABLE_DEBIT = YES
ENABLE_CREDIT = YES
@inline-secret@ exchange-accountcredentials-default ../secrets/exchange-accountcredentials-default.secret.conf
EOF


cat <<EOF >/etc/taler-exchange/secrets/exchange-accountcredentials-default.secret.conf
[exchange-accountcredentials-default]
WIRE_GATEWAY_URL = $PROTO://$BANK_DOMAIN$PORT_SUFFIX/accounts/exchange/taler-wire-gateway/
WIRE_GATEWAY_AUTH_METHOD = basic
USERNAME = exchange
PASSWORD = $(get_credential_pw bank/exchange)
EOF

if [[ $ENABLE_AUDITOR = 1 ]]; then
  # Make sandcastle exchange config available to auditor
  cp /etc/taler-exchange/conf.d/sandcastle-exchange.conf /etc/taler-auditor/conf.d/sandcastle-exchange.conf

  # We run the offline tooling as root, maybe in the future there should be
  # a separate user created by the Debian package for that.
  AUDITOR_PUB=$(taler-auditor-offline setup)

  cat <<EOF >/etc/taler-auditor/conf.d/sandcastle-auditor.conf
[auditor]
PUBLIC_KEY = $AUDITOR_PUB

[exchangedb]

$(dup_exchange_opt exchangedb IDLE_RESERVE_EXPIRATION_TIME)
$(dup_exchange_opt exchangedb LEGAL_RESERVE_EXPIRATION_TIME)
$(dup_exchange_opt exchangedb AGGREGATOR_SHIFT)
$(dup_exchange_opt exchangedb DEFAULT_PURSE_LIMIT)

[exchangedb-postgres]
$(dup_exchange_opt exchangedb-postgres CONFIG)

[exchange]
$(dup_exchange_opt exchange CURRENCY)
$(dup_exchange_opt exchange CURRENCY_ROUND_UNIT)
$(dup_exchange_opt exchange DB)


EOF
fi

# The config shipped with the package can conflict with the
# trusted sandcastle exchange if the currency is KUDOS.
rm -f /usr/share/taler-exchange/config.d/kudos.conf
rm -f /usr/share/taler-merchant/config.d/kudos.conf

# We need to define the default currency for the UI.
cat <<EOF >/etc/taler-merchant/conf.d/sandcastle-merchant.conf
[merchant]
CURRENCY = $CURRENCY
EOF

cat <<EOF >/etc/taler-merchant/conf.d/sandcastle-merchant-exchanges.conf
[merchant-exchange-sandcastle]
EXCHANGE_BASE_URL = $PROTO://$EXCHANGE_DOMAIN$PORT_SUFFIX/
MASTER_KEY = $MASTER_PUBLIC_KEY
CURRENCY = $CURRENCY
EOF

# Allow overrides to modify merchant config
[[ $(type -t hook_merchant_config) == function ]] && hook_merchant_config

# FIXME: This is a workaround, fix the packaging of taler-merchant-frontends here!
mkdir -p /etc/taler


cat <<EOF >/etc/taler/taler-merchant-frontends.conf
# Different entry point, we need to repeat some settings.
# In the future, taler-merchant-demos should become
# robust enough to read from the main config.
[taler]
CURRENCY = $CURRENCY

[frontend-demo-landing]
SERVE = http
HTTP_PORT = $PORT_INTERNAL_LANDING

[frontend-demo-blog]
SERVE = http
HTTP_PORT = $PORT_INTERNAL_BLOG
BACKEND_URL = $PROTO://$MERCHANT_DOMAIN$PORT_SUFFIX/instances/blog/
BACKEND_APIKEY = secret-token:$(get_credential_pw merchant/blog)

[frontend-demo-donations]
SERVE = http
HTTP_PORT = $PORT_INTERNAL_DONATIONS
BACKEND_URL_TOR = $PROTO://$MERCHANT_DOMAIN$PORT_SUFFIX/instances/tor/
BACKEND_APIKEY_TOR = secret-token:$(get_credential_pw merchant/tor)
BACKEND_URL_TALER = $PROTO://$MERCHANT_DOMAIN$PORT_SUFFIX/instances/taler/
BACKEND_APIKEY_TALER = secret-token:$(get_credential_pw merchant/taler)
BACKEND_URL_GNUNET = $PROTO://$MERCHANT_DOMAIN$PORT_SUFFIX/instances/gnunet/
BACKEND_APIKEY_GNUNET = secret-token:$(get_credential_pw merchant/gnunet)
EOF

# This really should not exist, the taler-merchant-frontends
# should be easier to configure!
cat <<EOF >/etc/taler/taler-merchant-frontends.env
TALER_ENV_URL_INTRO=$PROTO://$LANDING_DOMAIN$PORT_SUFFIX/
TALER_ENV_URL_LANDING=$PROTO://$LANDING_DOMAIN$PORT_SUFFIX/
TALER_ENV_URL_BANK=$PROTO://$BANK_DOMAIN$PORT_SUFFIX/
TALER_ENV_URL_MERCHANT_BLOG=$PROTO://$BLOG_DOMAIN$PORT_SUFFIX/
TALER_ENV_URL_MERCHANT_DONATIONS=$PROTO://$DONATIONS_DOMAIN$PORT_SUFFIX/
EOF

#
# Create databases
#

systemctl start postgresql.service

# Set up databases.
# Do that *before* we potentially do a restore-from-backup.

challenger-dbconfig

# Sets up the database for both libeufin-bank and libeufin-nexus.  We only need
# the libeufin-bank DB though.
libeufin-dbconfig

if [[ $ENABLE_AUDITOR = 1 ]]; then
  # Add auditor user to DB group *before* running taler-exchange-dbconfig,
  # so that DB permissions are adjusted accordingly.
  usermod taler-auditor-httpd -aG taler-exchange-db
  taler-auditor-dbconfig
fi

taler-exchange-dbconfig

taler-merchant-dbconfig


#
# Import backup if necessary.
#

if [[ -d /exported && -e /exported/import-request ]]; then
  echo "Importing databases"

  # FIXME: Consider backing up old DB before importing new one
  # FIXME: This is rather hacky, it would be better to use "pg_dump -Fc" and "pg_restore"
  sudo -u postgres dropdb taler-exchange
  sudo -u postgres dropdb taler-merchant
  sudo -u postgres dropdb libeufin

  sudo -u postgres createdb taler-exchange
  sudo -u postgres createdb taler-merchant
  sudo -u postgres createdb libeufin

  sudo -u postgres psql taler-exchange -f /exported/taler-exchange/taler-exchange.sql
  sudo -u postgres psql taler-merchant -f /exported/taler-merchant/taler-merchant.sql
  sudo -u postgres psql libeufin -f /exported/libeufin/libeufin.sql

  libeufin-dbconfig
  taler-exchange-dbconfig
  taler-merchant-dbconfig

  rm -rf /var/lib/taler-exchange/secmod-eddsa/*
  cp -r /exported/taler-exchange/secmod-eddsa/* /var/lib/taler-exchange/secmod-eddsa/

  rm -rf /var/lib/taler-exchange/secmod-rsa/*
  cp -r /exported/taler-exchange/secmod-rsa/* /var/lib/taler-exchange/secmod-rsa/

  rm -rf /var/lib/taler-exchange/secmod-cs/*
  cp -r /exported/taler-exchange/secmod-cs/* /var/lib/taler-exchange/secmod-cs/

  echo "Marking import as done"
  rm /exported/import-request
fi


# We need to adjust file ownership, as the container might have different user and group
# IDs than the volume. That can happen when the packages in the container are installed
# in a different order.
# This is only relevant for non-root ownership.
chown --recursive taler-exchange-offline:taler-exchange-offline /var/lib/taler-exchange/offline/* || true
chown --recursive taler-exchange-secmod-cs:taler-exchange-secmod /var/lib/taler-exchange/secmod-cs
chown --recursive taler-exchange-secmod-rsa:taler-exchange-secmod /var/lib/taler-exchange/secmod-rsa
chown --recursive taler-exchange-secmod-eddsa:taler-exchange-secmod /var/lib/taler-exchange/secmod-eddsa
chown root:taler-exchange-db /etc/taler-exchange/secrets/exchange-db.secret.conf
chown root:taler-auditor-httpd /etc/taler-auditor/secrets/auditor-db.secret.conf
chmod 440 /etc/taler-merchant/secrets/merchant-db.secret.conf
chown taler-merchant-httpd:root /etc/taler-merchant/secrets/merchant-db.secret.conf
chown root:taler-exchange-db /etc/taler-exchange/secrets/exchange-db.secret.conf
chown taler-exchange-wire:taler-exchange-db /etc/taler-exchange/secrets/exchange-accountcredentials-default.secret.conf


# Caddy configuration.
# We use the caddy reverse proxy with automatic
# internal TLS setup to ensure that the services are
# reachable inside the container without any external
# DNS setup under the same domain name and with TLS
# from inside the container.

systemctl stop caddy.service

cat <<EOF >/etc/caddy/Caddyfile

# Services that only listen on unix domain sockets
# are reverse-proxied to serve on a TCP port.

:$PORT_INTERNAL_EXCHANGE {
  reverse_proxy unix//run/taler-exchange/httpd/exchange-http.sock
}

:$PORT_INTERNAL_MERCHANT {
  reverse_proxy unix//run/taler-merchant/httpd/merchant-http.sock {
    # Set this, or otherwise wrong taler://pay URIs will be generated.
    header_up X-Forwarded-Proto "https"
  }
}

:$PORT_INTERNAL_BANK_SPA {
  root * /usr/share/libeufin/spa
  root /settings.json /etc/libeufin/
  file_server
}

:$PORT_INTERNAL_AUDITOR {
  reverse_proxy unix//run/taler-auditor/httpd/auditor-http.sock
}

:$PORT_INTERNAL_CHALLENGER {
  handle {
    reverse_proxy unix//run/challenger/httpd/challenger.http {
      # Set this, or otherwise wrong taler://pay URIs will be generated.
      header_up X-Forwarded-Proto "https"
    }
  }

  # Serve challenges via HTTP.
  # This is obviously completely insecure, but fine
  # for the demo sandcastle.
  handle_path /challenges/* {
    root * /tmp/challenges/
    file_server {
      browse
    }
  }
}
EOF

if [[ $PROTO = https ]]; then
  cat <<EOF >>/etc/caddy/Caddyfile

# Internally reverse-proxy https://,
# so that service can talk to each other via
# https:// inside the container.

https://$BANK_DOMAIN {
  tls internal
  reverse_proxy :8080 {
    # libeufin-bank should eventually not require this anymore,
    # but currently doesn't work without this header.
    header_up X-Forwarded-Prefix ""
  }
}

https://$EXCHANGE_DOMAIN {
  tls internal
  reverse_proxy unix//run/taler-exchange/httpd/exchange-http.sock
}

https://$MERCHANT_DOMAIN {
  tls internal
  reverse_proxy unix//run/taler-merchant/httpd/merchant-http.sock {
    # Set this, or otherwise wrong taler://pay URIs will be generated.
    header_up X-Forwarded-Proto "https"
  }
}

https://$AUDITOR_DOMAIN {
  tls internal
  reverse_proxy unix//run/taler-auditor/httpd/auditor-http.sock
}

https://$CHALLENGER_DOMAIN {
  tls internal
  reverse_proxy unix//run/challenger/httpd/challenger.http
}
EOF

else
  # Config for HTTP without TLS.

  cat <<EOF >>/etc/caddy/Caddyfile

http://$BANK_DOMAIN$PORT_SUFFIX {
  reverse_proxy :8080 {
    # libeufin-bank should eventually not require this anymore,
    # but currently doesn't work without this header.
    header_up X-Forwarded-Prefix ""
  }
}

http://$EXCHANGE_DOMAIN$PORT_SUFFIX {
  reverse_proxy unix//run/taler-exchange/httpd/exchange-http.sock
}

http://$MERCHANT_DOMAIN$PORT_SUFFIX {
  reverse_proxy unix//run/taler-merchant/httpd/merchant-http.sock
}

http://$AUDITOR_DOMAIN$PORT_SUFFIX {
  reverse_proxy unix//run/taler-auditor/httpd/auditor-http.sock
}

http://$CHALLENGER_DOMAIN$PORT_SUFFIX {
  reverse_proxy unix//run/challenger/httpd/challenger.http
}

http://$LANDING_DOMAIN$PORT_SUFFIX {
  reverse_proxy :$PORT_INTERNAL_LANDING
}

http://$BLOG_DOMAIN$PORT_SUFFIX {
  reverse_proxy :$PORT_INTERNAL_BLOG
}

http://$DONATIONS_DOMAIN$PORT_SUFFIX {
  reverse_proxy :$PORT_INTERNAL_DONATIONS
}
EOF

fi

cat <<EOF >>/etc/hosts
# Start of Taler Sandcastle Domains
127.0.0.1 $LANDING_DOMAIN
127.0.0.1 $BANK_DOMAIN
127.0.0.1 $EXCHANGE_DOMAIN
127.0.0.1 $MERCHANT_DOMAIN
127.0.0.1 $BLOG_DOMAIN
127.0.0.1 $DONATIONS_DOMAIN
127.0.0.1 $CHALLENGER_DOMAIN
# End of Taler Sandcastle Domains
EOF

systemctl start caddy.service

# Install local, internal CA certs for caddy
caddy trust

# Set up challenger

CHALLENGER_CLIENT_SECRET=secret-token:sandbox
CHALLENGER_CLIENT_ID=$(sudo -u challenger-httpd challenger-admin -q --add="$CHALLENGER_CLIENT_SECRET" https://$EXCHANGE_DOMAIN/kyc-proof/mychallenger)
echo Challenger client ID: $CHALLENGER_CLIENT_ID

systemctl enable --now challenger-httpd.service

# Set up bank

sudo -i -u libeufin-bank libeufin-bank edit-account admin --debit_threshold=$CURRENCY:1000000
sudo -i -u libeufin-bank libeufin-bank passwd admin $(get_credential_pw bank/admin)

systemctl enable --now libeufin-bank.service

BANK_BASEURL=$PROTO://$BANK_DOMAIN$PORT_SUFFIX/

taler-harness deployment wait-taler-service taler-corebank ${BANK_BASEURL}config

sudo -i -u libeufin-bank libeufin-bank passwd exchange $(get_credential_pw bank/exchange) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login exchange --exchange --public \
  --payto $EXCHANGE_PLAIN_PAYTO \
  --name Exchange \
  --password $(get_credential_pw bank/exchange)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-default $(get_credential_pw bank/merchant-default) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-default --public \
  --payto "payto://iban/$MERCHANT_IBAN_DEFAULT" \
  --name "Default Demo Merchant" \
  --password $(get_credential_pw bank/merchant-default)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-pos $(get_credential_pw bank/merchant-pos) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-pos --public \
  --payto "payto://iban/$MERCHANT_IBAN_POS" \
  --name "PoS Merchant" \
  --password $(get_credential_pw bank/merchant-pos)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-blog $(get_credential_pw bank/merchant-blog) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-blog --public \
  --payto "payto://iban/$MERCHANT_IBAN_BLOG" \
  --name "Blog Merchant" \
  --password $(get_credential_pw bank/merchant-blog)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-gnunet $(get_credential_pw bank/merchant-gnunet) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-gnunet --public \
  --payto "payto://iban/$MERCHANT_IBAN_GNUNET" \
  --name "GNUnet Donations Merchant" \
  --password $(get_credential_pw bank/merchant-gnunet)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-taler $(get_credential_pw bank/merchant-taler) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-taler --public \
  --payto "payto://iban/$MERCHANT_IBAN_TALER" \
  --name "Taler Donations Merchant" \
  --password $(get_credential_pw bank/merchant-taler)

sudo -i -u libeufin-bank libeufin-bank passwd merchant-tor $(get_credential_pw bank/merchant-tor) || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-tor --public \
  --payto "payto://iban/$MERCHANT_IBAN_TOR" \
  --name "Tor Donations Merchant" \
  --password $(get_credential_pw bank/merchant-tor)

# Special bank account without a secure password
sudo -i -u libeufin-bank libeufin-bank passwd merchant-sandbox sandbox || true
taler-harness deployment provision-bank-account "${BANK_BASEURL}" \
  --login merchant-sandbox --public \
  --payto "payto://iban/$MERCHANT_IBAN_SANDBOX" \
  --name "Sandbox Merchant" \
  --password sandbox

# Set up exchange

##
## Configure KYC if enabled
##

if [[ ${ENABLE_KYC:-0} == 1 ]]; then
  # KYC config
  cat <<EOF >/etc/taler-exchange/conf.d/sandcastle-kyc.conf
[exchange]
enable_kyc = yes

AML_SPA_DIALECT = $AML_SPA_DIALECT

[kyc-rule-r1]
OPERATION_TYPE = withdraw
ENABLED = yes
EXPOSED = yes
IS_AND_COMBINATOR = YES
THRESHOLD = $CURRENCY:10
TIMEFRAME = 1h
NEXT_MEASURES = m1 m2

[kyc-rule-r2]
OPERATION_TYPE = balance
ENABLED = yes
EXPOSED = yes
IS_AND_COMBINATOR = YES
THRESHOLD = $CURRENCY:100
TIMEFRAME = forever
NEXT_MEASURES = m1 m2

[kyc-measure-m1]
CHECK_NAME = c1
CONTEXT = {}
PROGRAM = p1

[aml-program-p1]
COMMAND = /data/sandcastle-amp-form
ENABLED = true
DESCRIPTION = test p1
FALLBACK = freeze

[kyc-check-c1]
TYPE = FORM
FORM_NAME = name_and_dob
DESCRIPTION = name and date of birth
OUTPUTS = full_name birthdate
FALLBACK = freeze

[kyc-measure-m2]
CHECK_NAME = c2
CONTEXT = {}
PROGRAM = p2

[kyc-measure-freeze]
CHECK_NAME = SKIP
CONTEXT = {}
PROGRAM = freeze

[aml-program-freeze]
COMMAND = taler-exchange-helper-measure-freeze
ENABLED = true
DESCRIPTION = freeze all operations on the account
FALLBACK = freeze

[aml-program-p2]
COMMAND = /data/sandcastle-amp-email
ENABLED = true
DESCRIPTION = check for validated email address in attributes
FALLBACK = freeze

[kyc-check-c2]
TYPE = LINK
PROVIDER_ID = mychallenger
DESCRIPTION = email verification via challenger
OUTPUTS = email
FALLBACK = freeze

#
# GLS KYC
#

[aml-program-nop]
COMMAND = /bin/true
ENABLED = true
DESCRIPTION = do nothing
FALLBACK = freeze

[kyc-measure-test-gls]
CHECK_NAME = form-gls-onboarding
PROGRAM = nop
CONTEXT = {}
VOLUNTARY = NO


[kyc-check-form-gls-onboarding]
TYPE = FORM
FORM_NAME = gls-onboarding
DESCRIPTION = "testing gls onboarding"
DESCRIPTION_I18N = {"de":"w"}
OUTPUTS = 
FALLBACK = freeze

[kyc-rule-test1]
OPERATION_TYPE = BALANCE
NEXT_MEASURES = test-gls
IS_AND_COMBINATOR = NO
EXPOSED = YES
THRESHOLD = $CURRENCY:1000010
TIMEFRAME = forever
ENABLED = YES

# end of GLS-style KYC

[kyc-provider-mychallenger]
LOGIC = oauth2
# This does not seem to be used, but required and documented?!
CONVERTER = /bin/true
KYC_OAUTH2_VALIDITY = 2d
KYC_OAUTH2_CLIENT_ID = $CHALLENGER_CLIENT_ID
KYC_OAUTH2_CLIENT_SECRET = $CHALLENGER_CLIENT_SECRET
KYC_OAUTH2_AUTHORIZE_URL = "$PROTO://$CHALLENGER_DOMAIN$PORT_SUFFIX/authorize#setup"
KYC_OAUTH2_TOKEN_URL = "$PROTO://$CHALLENGER_DOMAIN$PORT_SUFFIX/token"
KYC_OAUTH2_INFO_URL = "$PROTO://$CHALLENGER_DOMAIN$PORT_SUFFIX/info"
KYC_OAUTH2_POST_URL = "https://taler.net/en/kyc-done.html"
KYC_OAUTH2_CONVERTER_HELPER = taler-exchange-kyc-oauth2-challenger.sh
EOF

else
  rm -f /etc/taler-exchange/conf.d/sandcastle-kyc.conf
fi


if [[ ! -e /etc/taler-exchange/conf.d/sandcastle-$CURRENCY-coins.conf ]]; then
  # Only create if necessary, as each [COIN-...] section
  # has a unique name with a timestamp.
  taler-harness deployment gen-coin-config \
    --min-amount "${CURRENCY}:0.01" \
    --max-amount "${CURRENCY}:100" \
    >"/etc/taler-exchange/conf.d/sandcastle-$CURRENCY-coins.conf"
fi

taler-terms-generator -i /usr/share/taler-exchange/terms/exchange-tos-v0
taler-terms-generator -i /usr/share/taler-exchange/terms/exchange-pp-v0

systemctl enable --now taler-exchange.target

taler-harness deployment wait-taler-service taler-exchange $PROTO://$EXCHANGE_DOMAIN$PORT_SUFFIX/config
taler-harness deployment wait-endpoint $PROTO://$EXCHANGE_DOMAIN$PORT_SUFFIX/management/keys

sudo -i -u taler-exchange-offline \
  taler-exchange-offline \
  -c /etc/taler-exchange/taler-exchange.conf \
  download \
  sign \
  upload

sudo -i -u taler-exchange-offline \
  taler-exchange-offline \
  enable-account "${EXCHANGE_FULL_PAYTO}" \
  wire-fee now iban "${CURRENCY}":0 "${CURRENCY}":0 \
  global-fee now "${CURRENCY}":0 "${CURRENCY}":0 "${CURRENCY}":0 1h 6a 0 \
  upload

systemctl enable --now taler-exchange-offline.timer

function dup_exchange_opt() {
  echo "$2 = $(taler-exchange-config -c /etc/taler-exchange/taler-exchange.conf -s $1 -o $2)"
}

#
# Set up exchange auditor
#

if [[ $ENABLE_AUDITOR = 1 ]]; then
  systemctl enable --now taler-auditor.target
fi

# Set up merchant backend

MERCHANT_BASEURL=$PROTO://$MERCHANT_DOMAIN$PORT_SUFFIX/

systemctl enable --now taler-merchant.target
taler-harness deployment wait-taler-service taler-merchant ${MERCHANT_BASEURL}config

function reset_merchant_pw() {
  pw=secret-token:$(get_credential_pw merchant/$1)
  sudo -u taler-merchant-httpd taler-merchant-passwd --instance "$1" "$pw" || true
}

reset_merchant_pw default
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/default) \
  --name Merchant \
  --id default \
  --payto "payto://iban/$MERCHANT_IBAN_DEFAULT?receiver-name=Merchant"

reset_merchant_pw pos
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/pos) \
  --name "POS Merchant" \
  --id pos \
  --payto "payto://iban/$MERCHANT_IBAN_POS?receiver-name=POS+Merchant"

reset_merchant_pw blog
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/blog) \
  --name "Blog Merchant" \
  --id blog \
  --payto "payto://iban/$MERCHANT_IBAN_BLOG?receiver-name=Blog+Merchant"

reset_merchant_pw gnunet
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/gnunet) \
  --name "GNUnet Merchant" \
  --id gnunet \
  --payto "payto://iban/$MERCHANT_IBAN_GNUNET?receiver-name=GNUnet+Merchant"

reset_merchant_pw taler
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/taler) \
  --name "Taler Merchant" \
  --id taler \
  --payto "payto://iban/$MERCHANT_IBAN_TALER?receiver-name=Taler+Merchant"

reset_merchant_pw tor
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:$(get_credential_pw merchant/tor) \
  --name "Tor Merchant" \
  --id tor \
  --payto "payto://iban/$MERCHANT_IBAN_TOR?receiver-name=Tor+Merchant"

# Special instance with fixed "sandbox" password
sudo -u taler-merchant-httpd taler-merchant-passwd --instance sandbox secret-token:sandbox || true
taler-harness deployment provision-merchant-instance \
  ${MERCHANT_BASEURL} \
  --management-token secret-token:$(get_credential_pw merchant/default) \
  --instance-token secret-token:sandbox \
  --name "sandbox merchant" \
  --id sandbox \
  --payto "payto://iban/$MERCHANT_IBAN_SANDBOX?receiver-name=Sandbox+Merchant"

# Now we set up the taler-merchant-demos


systemctl enable --now taler-demo-landing
systemctl enable --now taler-demo-blog
systemctl enable --now taler-demo-donations

# FIXME: Maybe do some taler-wallet-cli test?
# FIXME: How do we report errors occurring during the setup script?
