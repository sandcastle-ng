This directory contains scripts and config files that are meant to be used on the host
of sandcastle, i.e. the machine running podman, and not *inside* sandcastle.
